package com.stg.makethon.login.entity;

public class DataDto {
    private String emailId;
    private String bluetoothAddr;
    private Long expiry;

    public DataDto(String emailId, String bluetoothAddr, Long expiry) {
        this.emailId = emailId;
        this.bluetoothAddr = bluetoothAddr;
        this.expiry = expiry;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getBluetoothAddr() {
        return bluetoothAddr;
    }

    public void setBluetoothAddr(String bluetoothAddr) {
        this.bluetoothAddr = bluetoothAddr;
    }

    public Long getExpiry() {
        return expiry;
    }

    public void setExpiry(Long expiry) {
        this.expiry = expiry;
    }

    @Override
    public String toString() {
        return "DataDto{" +
                "emailId='" + emailId + '\'' +
                ", bluetoothAddr='" + bluetoothAddr + '\'' +
                ", expiry=" + expiry +
                '}';
    }
}
