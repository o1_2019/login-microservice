package com.stg.makethon.login.entity;


public class SignedDataDto {
    private DataDto data;
    private String signature;

    public SignedDataDto(DataDto data, String signature) {
        this.data = data;
        this.signature = signature;
    }

    public DataDto getData() {
        return data;
    }

    public void setData(DataDto data) {
        this.data = data;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "SignedDataDto{" +
                "data='" + data.toString() + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }
}
