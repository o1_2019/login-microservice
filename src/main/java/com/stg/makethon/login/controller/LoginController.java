package com.stg.makethon.login.controller;

import com.stg.makethon.login.entity.SignedDataDto;
import com.stg.makethon.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LoginController {
    private LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/api/login/mobile/qrcode")
    public byte[] getQrCodeForKiosk(@RequestBody Object data) {
        return loginService.getSignedQrCode(data);
    }

    @PostMapping("/api/login/kiosk/qrcode/verification")
    public boolean validateKioskLogin(@RequestBody SignedDataDto signedDataDto) {
        return loginService.verifyQrCode(signedDataDto);
    }
}
