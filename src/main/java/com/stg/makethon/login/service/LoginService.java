package com.stg.makethon.login.service;

import com.stg.makethon.login.entity.SignedDataDto;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class LoginService {
    private static final String DIGI_SIG_URL = "http://localhost:8090/api/digisig/signature";
    private static final String DIGI_SIG_VERIFY_URL = "http://localhost:8090/api/digisig/verification";
    private static final String QR_CODE_URL = "http://localhost:8091/api/qrcode/new";

    private Object digitallySignData(Object data) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForObject(DIGI_SIG_URL, data, Object.class);
    }

    private byte[] generateQrCode(Object data) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForObject(QR_CODE_URL, data, byte[].class);
    }

    public byte[] getSignedQrCode(Object data) {
        Object signedData = digitallySignData(data);
        return generateQrCode(signedData);
    }

    public Boolean verifyQrCode(SignedDataDto data) {
        RestTemplate restTemplate = new RestTemplate();
        System.out.println(data);
        long expiry = data.getData().getExpiry();
        boolean isValid = expiry > System.currentTimeMillis();
        System.out.println("Expiry: " + expiry + ":: Current: " + System.currentTimeMillis());
        Boolean isVerified = restTemplate.postForObject(DIGI_SIG_VERIFY_URL, data, Boolean.class);
        return isValid && isVerified;
    }
}
